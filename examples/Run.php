<?php

include 'Hetzner.php';

$request = curl_init();
curl_setopt($request, CURLOPT_URL,'https://www.hetzner.com/a_hz_serverboerse/live_data.json');
curl_setopt($request,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36');
curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
curl_setopt($request, CURLOPT_SSL_VERIFYPEER, true);
curl_setopt($request, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($request, CURLOPT_CONNECTTIMEOUT ,2);
curl_setopt($request, CURLOPT_TIMEOUT, 2);
$result = curl_exec($request);
curl_close($request);

$Hetzner = new Hetzner();
print_r($Hetzner->Filter($result));

?>
